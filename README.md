# tk1
[![pipeline status](https://gitlab.com/tsanaapatricia/tk1/badges/master/pipeline.svg)](https://gitlab.com/tsanaapatricia/tk1/-/commits/master)
[![coverage report](https://gitlab.com/tsanaapatricia/tk1/badges/master/coverage.svg)](https://gitlab.com/tsanaapatricia/tk1/-/commits/master)

Repositori ini berisi proyek TK PPW kami yang akan dideploy ke heroku.

## Anggota
Kami dari kelompok 10 kelas PPW D beranggotakan :
- Muhammad Daffa Syammary 1906399146
- Tsanaa Patricia Khonsaa 1906353914
- Assyifa Raudina 1906399152
- Nadya Aprillia 1906398566

## Link Herokuapp
- http://ngapaini.herokuapp.com/

## Link Prototype
- https://www.figma.com/file/VASKiI7kWQ5d3Gx5uoMFuW/Prototype-TK?node-id=5%3A515

## Behind the app
   dalam keadaan covid-19 dimana aktivitas terbatas setiap individu merasakan bosan setidaknya sekali dalam karantina. Kami ingin membuat       aplikasi yang dapat menginspirasi dan memberi ide untuk mengisi banyaknya waktu luang yang didapatkan dalam masa pandemi ini.
Dalam aplikasi ini anda dapat melihat aktivitas yang sedang banyak dilakukan oleh user saat ini dari conten yang dipost oleh user.
Kita juga dapat mempost sendiri ide atau aktivitas yang ingin dishare dengan membuat akun terlebih dahulu.

## Features
- post
- navbar 
- filter
- signup
- login
- addpost
- kudos
- comment





