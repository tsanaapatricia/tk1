from django.shortcuts import render, redirect
from .models import Like
from form.models import PostModel
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required

def home(request):
    count_post = PostModel.objects.all().count()
    postingan = {}
    for pos in PostModel.objects.all():
        if request.user.is_authenticated :
            pos_satuan = {
                "post" : pos,
                "liked" : pos.like_set.filter(user_kudos=request.user) and True or False,
                "kudos" : pos.like_set.all().count()
            }
        postingan[pos.id] = pos_satuan
    postingan1 = {
        'count_post' : count_post,
        'postingan' : postingan
    }
    print(postingan1)
    return render(request, 'main/home.html', postingan1)

@login_required
def kudos_or_undo(request, post_id):
    new_kudos, like_it = Like.objects.get_or_create(user_kudos=User, post_kudos=PostModel.objects.get(id=post_id))
    if not like_it:
        new_kudos.delete()
    return redirect('../')