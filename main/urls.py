from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('kudos/<int:index>/', views.kudos_or_undo, name='kudos')
]
