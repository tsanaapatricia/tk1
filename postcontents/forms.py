from django import forms

from .models import Comment

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['komen']
        widgets = {
            'komen': forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Comment'
                                            })
        }
        labels = {
        "komen": ""
        }