from django.test import LiveServerTestCase, TestCase, tag,Client
from django.urls import reverse, resolve

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .apps import PostcontentsConfig
from .views import contents, otherprofile
from .models import Comment
from .forms import CommentForm
from form.models import PostModel
from users.models import Profile
from PIL import Image

from selenium import webdriver

class PostcontentsConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(PostcontentsConfig.name,'postcontents')

class TestModels(TestCase):
    def setUp(self):
        self.new_user_one = User.objects.create_user("Anonymous1",password='testpasstest123')
        self.new_user_two = User.objects.create_user("Anonymous2",password='testpasstest123')
        self.new_user_two.save()
        self.new_user_one.save()
        self.image_url = "profile_pics/dying.jpg"        
        self.new_profile = Profile.objects.create(
            user = self.new_user_two,
            image = self.image_url
        )
        self.new_profile.save()
        self.title = "Woobadubba Testaa"
        self.content = "anything, just anything"
        self.category = "Untalented"
        self.new_post = PostModel.objects.create(
            title = self.title,
            content = self.content,
            category = self.category,
            author = self.new_user_one,
        )
        self.new_post.save()
        self.comen = "nice recipe! just tried it and it totally worked blah blah"
        self.new_comment = Comment.objects.create(
            user = self.new_user_two,
            komen = self.comen,
            postingan = self.new_post,
        )
        self.new_comment.save()

    def test_comment_instance(self):
        self.assertEqual(Comment.objects.count(),1)

    def test_comment_instance_is_correct(self):
        self.assertEqual(Comment.objects.first().user,self.new_user_two)
        self.assertEqual(Comment.objects.first().komen,self.comen)
        self.assertEqual(Comment.objects.first().postingan,self.new_post)

    def test_comment_instance_to_string(self):
        self.assertEqual(self.new_comment._str_(), self.comen)


class TestViewsandForm(TestCase):
    def setUp(self):
        self.client = Client()
        self.new_user_one = User.objects.create_user("Anonymous1",password='testpasstest123')
        self.new_user_two = User.objects.create_user("Anonymous2",password='testpasstest123')
        self.new_user_one.save()
        self.new_user_two.save()
        self.image_url = "profile_pics/dying.jpg"        
        self.new_profile = Profile.objects.create(
            user = self.new_user_two,
            image = self.image_url
        )
        self.new_profile.save()
        self.title = "Woobadubba Testaa"
        self.content = "anything, just anything"
        self.category = "Untalented"
        self.new_post = PostModel.objects.create(
            title = self.title,
            content = self.content,
            category = self.category,
            author = self.new_user_one,
        )
        self.new_post.save()
        self.comen = "test"
        self.url_details = "/postcontents/contents/1/"

    def test_comment_form_blank_validation(self):
        form = CommentForm(data={
            "user" : self.new_user_two,
            "komen" : "",
            "postingan" : self.new_post,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['komen'],
            ["This field is required."]
        )

    def test_url_exists(self):
        response = self.client.get(self.url_details)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"content.html")

    
    def test_comment_POST_error_and_render_result(self):
        komen_example = "blegh blegh blegh"
        response_post = self.client.post(self.url_details, {
            "komen" : "",
        })
        self.assertEqual(response_post.status_code, 200)

        response = self.client.get(self.url_details)
        html_response = response.content.decode('utf8')
        self.assertNotIn(komen_example, html_response)

    def test_comment_user_not_authenticated(self):
        response = self.client.post(self.url_details, {
            "komen" : self.comen,
        })
        html_response = response.content.decode('utf8')
        self.assertIn("login required!", html_response)


    def test_comment_POST_success_and_render_result(self):
        self.client.post('/login/',{
            'username': 'Anonymous2',
            'password': 'testpasstest123'})
        response = self.client.post(self.url_details, {
            "komen" : self.comen,
        })
        print("==OBJEK KOMEN SETELAH POST= "+ str(Comment.objects.count()))
        self.assertEquals(response.status_code,200)
        objhasil = Comment.objects.first()
        self.assertEqual(objhasil.user, self.new_user_two)
        self.assertEqual(objhasil.postingan, self.new_post)
        self.assertEqual(objhasil.komen, self.comen)

        response = self.client.get(self.url_details)
        html_response = response.content.decode('utf8')
        self.assertIn(self.comen, html_response)
        self.assertIn(self.new_user_two.username, html_response)


    def test_lab5_using_index_func(self):
        print(Profile.objects.all())
        found = resolve("/postcontents/otherprofile/Anonymous2/")
        self.assertEqual(found.func, otherprofile)

        profileresponse = self.client.get("/postcontents/otherprofile/Anonymous1/")
        self.assertTemplateUsed(profileresponse, 'user/profile.html')
        self.assertTrue(200, profileresponse.status_code)
        html_response_this = profileresponse.content.decode('utf8')
        self.assertIn("Anonymous1", html_response_this)











