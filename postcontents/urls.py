from django.urls import path

from . import views
app_name = 'postcontents'

urlpatterns =[
    path('contents/<int:id>/', views.contents, name='contents'),
    path('otherprofile/<str:parameter>/', views.otherprofile, name='otherprofile')
] 