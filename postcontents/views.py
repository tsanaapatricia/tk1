from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import messages
from form.models import PostModel
from users.models import Profile
from .forms import CommentForm
from .models import Comment

# Create your views here.

def contents(request, id):
    postobj = PostModel.objects.get(id=id)
    form = CommentForm(request.POST or None)

    if form.is_valid():
        if not request.user.is_authenticated:
            messages.warning(request, "login required!")
        else:
            instance = form.save(commit=False)
            instance.postingan = postobj
            instance.user = request.user
            instance.save()
        form = CommentForm()

    comments = Comment.objects.all()
    profiles = Profile.objects.all()
    
    context = {
        'postcontent': postobj,
        'comments': comments,
        'commentform': form,
    }
    return render(request, 'content.html', context)

def otherprofile(request, parameter):
    author = User.objects.get(username=parameter)   
    context = {
        'posts' : PostModel.objects.filter(author=author),
        'author': author
    }
    return render(request, 'user/profile.html', context)



