from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormPost
from .models import PostModel
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def add(request):
    form = FormPost(request.POST or None)

    if form.is_valid():
        if request.method == 'POST':
            PostModel.objects.create(
                title = form.cleaned_data.get('title'),
                content = form.cleaned_data.get('content'),
                category = form.cleaned_data.get('category'),
                author = request.user 
            )
            return HttpResponseRedirect("/form/index/")

    context = {
        'page_title':'Add Content',
        'form':form
    }
    return render(request, 'add.html', context)

def index(request):
    post = PostModel.objects.all()
    context = {
        'post':post,
    }
    print(PostModel.objects.all())
    print(PostModel.objects.get(id=1))
    return render(request, 'index.html', context)