from django.urls import path

from . import views
app_name = 'form'

urlpatterns =[
    path('add/', views.add, name='add'),
    path('index/', views.index, name='index'),
] 